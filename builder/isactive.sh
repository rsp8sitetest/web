#!/bin/bash

set -e

topdir=$(echo $1 | cut -d'/' -f6- | cut -d'/' -f1)
test -z $topdir && test "rsp" = $2 && echo -n 'class="active" '
test -n $topdir && test $1 = $2 && echo -n 'class="active" '
