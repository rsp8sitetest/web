# Respawn Forever

Imagine a world where you could replace your body parts when they're
broken. A world where everybody has access to affordable and safe
replacement parts. No one dies waiting for an organ donor and
people are in charge of these open source, freedom respecting
parts, not some malicious proprietary company.

Pretty neat, huh?
That's what we do. And you should join us too, because why not?

* [GitLab](https://gitlab.com/rsp8)
* [Twitter](https://twitter.com/rsp8com)
